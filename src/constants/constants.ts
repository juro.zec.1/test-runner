import { Framework } from "../types";

export const FRAMEWORKS: Record<string, Framework> = {
  react: {
    name: "React v17.0.2",
    hex: "#61dafb",
    url: "https://benchmark-react-vite.vercel.app/",
  },
  svelte: {
    name: "Svelte v3.44.0",
    hex: "#ff3e00",
    url: "https://benchmark-svelte-vite.vercel.app/",
  },
};

export const PUPPETEER_VERSION = "v13.5.2";
export const HOST = "AMD Ryzen 7 6800H, 16GB RAM, Windows 11 Pro";
export const NODE = "Node v16.13.1 (LTS)";

export const INITIALIZE_SELECTOR = "#initialize";
export const FIRST_ROW_SELECTOR = "#main > table > tbody > tr:nth-child(1)";
export const IS_DONE_SELECTOR = "#isDone";

export const RESULTS_DIR = "results";
