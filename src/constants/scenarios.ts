import { Scenario } from "../types";

const create1KScenario: Scenario = {
  name: "Create 1.000 rows",
  selector: "#create1K",
  initialize: false,
  repCount: 10,
  warmupRounds: 5,
};

const create2KScenario: Scenario = {
  name: "Create 2.000 rows",
  selector: "#create2K",
  initialize: false,
  repCount: 10,
  warmupRounds: 5,
};

const create5KScenario: Scenario = {
  name: "Create 5.000 rows",
  selector: "#create5K",
  initialize: false,
  repCount: 10,
  warmupRounds: 5,
};

const create10KScenario: Scenario = {
  name: "Create 10.000 rows",
  selector: "#create10K",
  initialize: false,
  repCount: 10,
  warmupRounds: 5,
};

const create25KScenario: Scenario = {
  name: "Create 25.000 rows",
  selector: "#create25K",
  initialize: false,
  repCount: 10,
  warmupRounds: 5,
};

const updateEvery10thScenario: Scenario = {
  name: "Update every 10th row (r = 1000)",
  selector: "#updateEvery10th",
  initialize: true,
  repCount: 10,
  warmupRounds: 0,
};

const swapTwoScenario: Scenario = {
  name: "Swap 2 rows",
  selector: "#swapTwo",
  initialize: true,
  repCount: 10,
  warmupRounds: 0,
};

const deleteOneScenario: Scenario = {
  name: "Delete 1 row",
  selector: "#deleteOne",
  initialize: true,
  repCount: 10,
  warmupRounds: 0,
};

const deleteEvery3rdScenario: Scenario = {
  name: "Delete every 3rd row (r = 1000)",
  selector: "#deleteEvery3rd",
  initialize: true,
  repCount: 10,
  warmupRounds: 0,
};

const deleteAllScenario: Scenario = {
  name: "Delete all rows (r = 1000)",
  selector: "#deleteAll",
  initialize: true,
  repCount: 10,
  warmupRounds: 0,
};

export const SCENARIOS: Scenario[] = [
  create1KScenario,
  create2KScenario,
  create5KScenario,
  create10KScenario,
  create25KScenario,
  updateEvery10thScenario,
  swapTwoScenario,
  deleteOneScenario,
  deleteEvery3rdScenario,
  deleteAllScenario,
];
