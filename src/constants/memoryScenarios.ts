import { Scenario } from "../types";

const measureUsedJSHeapSizeBeforeInit: Scenario = {
  name: "Measure used heap size at startup",
  initialize: false,
  measureMemory: true,
  repCount: 10,
};

const measureUsedJSHeapSizeAfterInit: Scenario = {
  name: "Measure used heap size after creating 1.000 rows",
  initialize: true,
  measureMemory: true,
  repCount: 10,
};

export const MEMORY_SCENARIOS: Scenario[] = [
  measureUsedJSHeapSizeBeforeInit,
  measureUsedJSHeapSizeAfterInit,
];
