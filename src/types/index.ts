export type Scenario = {
  name: string;
  selector?: string;
  repCount: number;
  measureMemory?: boolean;
  initialize?: boolean;
  warmupRounds?: number;
};

export type DurationMillis = number;
export type UsedJSHeapSizeBytes = number;

export type BenchmarkResult = {
  mean: number;
  median: number;
  standardDeviation: number;
  min: number;
  max: number;
};

export type ScenarioResult = Scenario &
  BenchmarkResult & {
    durations: DurationMillis[];
    usedJSHeapSizes: UsedJSHeapSizeBytes[];
  };

export type Framework = {
  name: string;
  hex: string;
  url: string;
};

export type TestSuiteMetadata = {
  framework: string;
  puppeteerVersion: string;
  host: string;
  nodejs: string;
  cpuThrottlingFactor: number;
  scenarioCount: number;
  startedAt: string;
  finishedAt: string;
};
