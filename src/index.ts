import puppeteer, { Page } from "puppeteer";
import parseArgs from "minimist";
import chalk from "chalk";
import {
  DurationMillis,
  Framework,
  UsedJSHeapSizeBytes,
  Scenario,
  ScenarioResult,
  TestSuiteMetadata,
} from "./types";
import { computeMean, computeMedian, computeStandardDeviation } from "./utils";
import * as fs from "fs";
import {
  FIRST_ROW_SELECTOR,
  FRAMEWORKS,
  HOST,
  INITIALIZE_SELECTOR,
  IS_DONE_SELECTOR,
  NODE,
  PUPPETEER_VERSION,
  RESULTS_DIR,
  SCENARIOS,
  MEMORY_SCENARIOS,
} from "./constants";

(async () => {
  console.log("\n\n\ntest-runner started");
  console.log("\n");
  const {
    framework,
    throttle,
    "memory-suite": memorySuite,
  } = parseArgs(process.argv.slice(2));
  const scenarios = memorySuite ? MEMORY_SCENARIOS : SCENARIOS;
  if (!FRAMEWORKS[framework])
    throw new Error(
      'Invalid arguments. Framework should be one of "react" | "svelte"'
    );
  if (Number.isNaN(parseFloat(throttle)) || parseFloat(throttle) < 1)
    throw new Error(
      "Invalid arguments. Throttling factor should be equal or more than 1"
    );
  await startAll(FRAMEWORKS[framework], scenarios, throttle);
})()
  .then(() => console.log("test-runner finished successfully."))
  .catch((err) => console.error("test-runner encountered error:", err));

async function startAll(
  framework: Framework,
  scenarios: Scenario[],
  cpuThrottlingFactor: number
) {
  const results: ScenarioResult[] = [];
  const browser = await puppeteer.launch();
  const page: Page = await browser.newPage();
  await page.emulateCPUThrottling(cpuThrottlingFactor);
  const startedAt = new Date().toLocaleTimeString();
  const puppeteerVersion = `${PUPPETEER_VERSION} (${await page
    .browser()
    .version()})`;

  console.log("Framework:", chalk.hex(framework.hex).bold(framework.name));
  console.log("Puppeteer version:", chalk.yellow(puppeteerVersion));
  console.log("Host:", chalk.yellow(HOST));
  console.log("Node.js:", chalk.yellow(NODE));
  console.log(
    "CPU throttling factor:",
    chalk.yellow(
      cpuThrottlingFactor,
      cpuThrottlingFactor === 1 ? "(no throttle)" : ""
    )
  );
  console.log("Number of scenarios:", chalk.yellow(scenarios.length), "\n");

  await page.goto(framework.url, { waitUntil: "networkidle2" });

  for (const [index, scenario] of scenarios.entries()) {
    const result = await runScenario(page, scenario, index + 1);
    results.push(result);
  }

  console.log("\n");
  const metadata: TestSuiteMetadata = {
    framework: framework.name,
    puppeteerVersion,
    host: HOST,
    nodejs: NODE,
    cpuThrottlingFactor,
    scenarioCount: scenarios.length,
    startedAt,
    finishedAt: new Date().toLocaleTimeString(),
  };
  saveResults(results, metadata);

  await browser.close();
}

async function runScenario(
  page: Page,
  scenario: Scenario,
  order: number
): Promise<ScenarioResult> {
  console.log(
    `\n${order}.`,
    "scenario",
    chalk.yellowBright.bold(scenario.name)
  );
  let duration: DurationMillis;
  let usedJSHeapSize: UsedJSHeapSizeBytes;
  const durations: DurationMillis[] = [];
  const usedJSHeapSizes: UsedJSHeapSizeBytes[] = [];
  for (let i = 0; i < scenario.repCount; i++) {
    process.stdout.write("█ ");
    if (scenario.measureMemory) {
      usedJSHeapSize = await measureMemory(page, scenario);
      usedJSHeapSizes.push(usedJSHeapSize);
    } else {
      duration = await runBenchmark(page, scenario);
      durations.push(duration);
    }
  }
  console.log(chalk.greenBright.bold("OK"));
  const benchmarkResult = analyzeBenchmark(
    scenario.measureMemory ? usedJSHeapSizes : durations
  );
  return {
    ...scenario,
    ...benchmarkResult,
    durations,
    usedJSHeapSizes,
  };
}

async function measureMemory(
  page: Page,
  { initialize }: Scenario
): Promise<number> {
  await page.reload({ waitUntil: "networkidle2" });
  if (initialize) {
    await page.click(INITIALIZE_SELECTOR);
    await page.waitForSelector(FIRST_ROW_SELECTOR);
  }
  await new Promise((resolve) => setTimeout(resolve, 3000));
  const { JSHeapUsedSize } = await page.metrics();
  return JSHeapUsedSize || NaN;
}

async function runBenchmark(
  page: Page,
  { selector, initialize }: Scenario
): Promise<DurationMillis> {
  if (!selector) throw new Error("No selector provided");
  await page.reload({ waitUntil: "networkidle2" });
  if (initialize) {
    await page.click(INITIALIZE_SELECTOR);
    await page.waitForSelector(FIRST_ROW_SELECTOR);
  }
  await page.tracing.start();
  await page.click(selector);
  await page.waitForSelector(IS_DONE_SELECTOR);
  await page.tracing.stop();

  const performanceEntry = await page.evaluate(() =>
    JSON.stringify(performance.getEntriesByName("benchmark"))
  );
  return JSON.parse(performanceEntry)?.[0]?.duration;
}

function analyzeBenchmark(measurements: number[]) {
  return {
    mean: computeMean(measurements),
    median: computeMedian(measurements),
    standardDeviation: computeStandardDeviation(measurements),
    min: Math.min(...measurements),
    max: Math.max(...measurements),
  };
}

function saveResults(results: ScenarioResult[], metadata: TestSuiteMetadata) {
  const resultsPath = `${RESULTS_DIR}/result_${Date.now()}.json`;
  if (!fs.existsSync(RESULTS_DIR)) fs.mkdirSync(RESULTS_DIR);
  console.log("writing results to", chalk.yellow(resultsPath));
  fs.writeFileSync(
    resultsPath,
    JSON.stringify({ ...metadata, results }, null, 2)
  );
}
