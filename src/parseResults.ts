import parseArgs from "minimist";
import fs from "fs";
import { ScenarioResult } from "./types";

const round = (num: number) => Math.round((num + Number.EPSILON) * 100) / 100;

// Command: node dist/parseResults.js --result-file result_1651452210467.json
(async () => {
  console.log("\n\n\nparsing results ...\n");
  const { "result-file": resultFile } = parseArgs(process.argv.slice(2));
  console.log({ resultFile });
  if (!resultFile) throw new Error("No file name provided.");

  const resultData = JSON.parse(
    fs.readFileSync(`./results/${resultFile}`).toString()
  );
  resultData.results.forEach((result: ScenarioResult) => {
    const mean = round(result.median);
    const std = Math.ceil(result.standardDeviation);
    console.log(`${result.name} |`, mean, `± ${std}ms`);
  });
})()
  .then(() => console.log("parsing results finished successfully."))
  .catch((err) => console.error("parsing results encountered error:", err));
