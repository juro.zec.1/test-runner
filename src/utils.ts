export function computeMedian(numbers: number[]): number {
  const sorted = numbers.slice().sort((a, b) => a - b);
  const middle = Math.floor(sorted.length / 2);
  if (sorted.length % 2 === 0) return (sorted[middle - 1] + sorted[middle]) / 2;
  return sorted[middle];
}

export function computeMean(numbers: number[]): number {
  return numbers.reduce((a, b) => a + b) / numbers.length;
}

export function computeStandardDeviation(numbers: number[]): number {
  const mean = computeMean(numbers);
  return Math.sqrt(
    numbers.map((a) => Math.pow(a - mean, 2)).reduce((a, b) => a + b) /
      numbers.length
  );
}
